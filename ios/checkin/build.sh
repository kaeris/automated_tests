#!/bin/bash

source "$HOME/.rvm/scripts/rvm"
[[ -s ".rvmrc" ]] && source .rvmrc
# Set "fail on error" in bash
set -e
#rvm list

rvm use 2.2.2

cd ios/checkin/
bundle install

case $TESTFEATURES in
	"")
		#bundle exec cucumber --format json --out cucumber.json features
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip features
		;;
	"Run Full Test Suite")
		#bundle exec cucumber --format json --out cucumber.json features
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip features
		;;
	"1. Request Trial")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @1
		;;
	"2. Login")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @2
		;;
	"3. About")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @3
		;;
	"4. Contacts")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @4
		;;
	"5. Report a Problem")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @5
		;;
	"6. Select an Event")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @6
		;;
	"7. Adding Attendees")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @7
		;;
	"8. Attendees Page")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @8
		;;
	"9. Search Attendees")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @9
		;;
	"10. Sort Attendees")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @10
		;;
	"11. Filter Attendees")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @11
		;;
	"12. Delete Attendees")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @12
		;;
	"13. Attendee Info Page")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @13
		;;
	"14. Edit Attendee Information Page")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @14
		;;
	"15. Topics")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @15
		;;
	"16. Follow Up Actions")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @16
		;;
esac