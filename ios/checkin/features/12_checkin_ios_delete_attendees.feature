@checkin_feature_deleting_attendees @wip @12
Feature: Deleting Attendees

# Add contacts??

@12.1 @wip
Scenario: 12.1 Delete an attendee

@12.2 @wip
Scenario: 12.2 Delete 2 attendees and then log out and log back in

@12.3 @wip
Scenario: 12.3 Search for an attendee and then delete the attendee

@12.4 @wip
Scenario: 12.4 Filter an attendee and then delete the attendee

@12.5 @wip
Scenario: 12.5 Add a new attendee and then delete the attendee