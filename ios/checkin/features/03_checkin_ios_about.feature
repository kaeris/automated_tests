# encoding: UTF-8

@checkin_feature_about_tab @qa_ready @3
Feature: About Tab

@3.1
Scenario: 3.1 Home button from About
   Given the user is on the Login screen of the Check-In app
   And the user successfully logs in as "QA"
   And the user goes to the About page
   And the user clicks the Home button
   Then the user is on the landing page


# Currently does work since the menu isn't seen by Appium.
@3.2 @wip
Scenario: 3.2 Menu icon from About
   Given the user is on the Login screen of the Check-In app
   And the user successfully logs in as "QA"
   And the user goes to the About page
   And the user clicks the Menu Icon
   Then the app menu should open and "About" should be highlighted


@3.3
Scenario: 3.3 The About tab displays the app version
   Given the user is on the Login screen of the Check-In app
   And the user successfully logs in as "QA"
   And the user goes to the About page
   Then the correct app version is displayed


@3.4 @wip
Scenario: 3.4 Click Terms and Conditions link
  Given the user is on the Login screen of the Check-In app
  And the user successfully logs in as "QA"
  And the user goes to the About page
  And the user clicks the Terms and Conditions link
  Then default web browser should open and display the Terms and Conditions page