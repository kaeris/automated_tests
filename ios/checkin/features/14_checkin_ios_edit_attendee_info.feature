# encoding: UTF-8

@checkin_feature_edit_attendee @qa_ready @14
Feature: Edit Attendee Information Page

@14.1
Scenario: 14.1 Click X icon on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks the X button
	Then the Attendee Information page for "Hammond" will appear


@14.2
Scenario: 14.2 Click Edit Topics on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page


@14.3
Scenario: 14.3 Click Edit Follow-Up Actions on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page


@14.4
Scenario: 14.4 Click Cancel on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks "Cancel"
	Then the Attendee Information page for "Hammond" will appear


@14.5
Scenario: 14.5 Click Done on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 2"


@14.6
Scenario: 14.6 Edit attendee without last name
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Last Name*" from "Hammond" to ""
	And the user clicks "Done"
	Then the user will receive an "Invalid Last Name. Please try again." message
	#When the user clicks "OK"
	#Then the Edit Attendee Information page for "Hammond" will appear


@14.7
Scenario: 14.7 Edit attendee without company
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Company*" from "Top Gear" to ""
	And the user clicks "Done"
	Then the user will receive an "Invalid Company. Please try again." message
	#When the user clicks "OK"
	#Then the Edit Attendee Information page for "Hammond" will appear


@14.8
Scenario: 14.8 Edit attendee without email
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Email*" from "hammond@cars.com" to ""
	And the user clicks "Done"
	Then the user will receive an "Invalid Email. Please try again." message
	#When the user clicks "OK"
	#Then the Edit Attendee Information page for "Hammond" will appear


@14.9
Scenario: 14.9 Edit attendee with invalid email
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Email*" from "hammond@cars.com" to "?????????!!!"
	And the user clicks "Done"
	Then the user will receive an "Invalid Email. Please try again." message
	#When the user clicks "OK"
	#Then the Edit Attendee Information page for "Hammond" will appear


@14.10
Scenario: 14.10 Edit attendee and press cancel
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Company*" from "Top Gear" to "Amazon.com"
	And the user clicks "Cancel"
	Then the Attendee Information page for "Hammond" will appear
	And the "Company" field should contain "Top Gear"


@14.11
Scenario: 14.11 Edit attendee and press done
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Company*" from "Top Gear" to "Amazon.com"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Company" field should contain "Amazon.com"

# Currently cannot automate bc cannot tell if a topic/fua is selected or not.
@14.12 @wip
Scenario: 14.12 Edit Topics from Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Rosberg"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Rosberg" will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Rosberg" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Ruby" topics
	Then the "Ruby" topics should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Rosberg"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the Topics field should contain "Ruby"


# Currently cannot automate bc cannot tell if a topic/fua is selected or not.
@14.13 @wip
Scenario: 14.13 Edit Sub Topics from Edit Attendee page

# Currently cannot automate bc cannot tell if a topic/fua is selected or not.
@14.14 @wip
Scenario: 14.14 Edit Follow-Up Actions from Edit Attendee page


# Currently fails due to CEA-3330. Cannot save a note.
@14.15 @wip
Scenario: 14.15 Edit Note/Free Text
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "Free Text"
	And the user changes the text from "default free text" to "I edited this free text"
	And the user clicks "Note"
  	And the user changes the text from "Some random comments." to "I edited this note"
  	And the user clicks "Done"
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "Free Text"
	Then the text should match "I edited this free text"
	When the user clicks "Note"
  	Then the text should match "I edited this note"


# Currently fails due to CEA-3330. Cannot save a note.
@14.16 @wip
Scenario: 14.16 Edit existing Note/Free Text
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "Free Text"
	And the user changes the text from "I edited this free text" to "And on that bombshell"
	And the user clicks "Note"
  	And the user changes the text from "I edited this note" to "Some say..."
  	And the user clicks "Done"
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "Free Text"
	Then the text should match "And on that bombshell"
	When the user clicks "Note"
  	Then the text should match "Some say..."


# Currently fails. Waiting on bugfixes and new feature support for automation.
@14.17 @wip
Scenario: 14.17 Clear an existing note
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "Note"
  	And the user changes the text from "I edited this note" to ""
  	And the user clicks "Done"
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Comments" field should be empty


