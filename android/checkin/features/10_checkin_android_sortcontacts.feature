# encoding: UTF-8

@checkin_feature_sort_contacts @wip @10
Feature: Sort Contacts

# These sorting tests currently do not work due to an open Appium bug that does not allow us to check the value of the content-desc attribute of an element.
# This is the attribute that tells us whether the sort order is ascending or descending.
@wip @10.1
Scenario: 10.1 Choose a contact after sorting contacts by Last Name
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   And the contacts "Last Name" should be in "ascending" alphabetical order
   When the user sorts the contacts by "Last Name" into "descending" alphabetical order
   Then the contacts "Last Name" should be in "descending" alphabetical order
   When the user sorts the contacts by "Last Name" into "ascending" alphabetical order
   Then the contacts "Last Name" should be in "ascending" alphabetical order
   When the user clicks the contact "Messi"
   Then the Contact info panel should appear
   And the contact information for the contact should match "Messi" "eyes"

@wip @10.2
Scenario: 10.2 Sort Contacts by First Name
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   And the contacts "Last Name" should be in "ascending" alphabetical order
   When the user sorts the contacts by "First Name" into "descending" alphabetical order
   Then the contacts "First Name" should be in "descending" alphabetical order
   When the user sorts the contacts by "First Name" into "ascending" alphabetical order
   Then the contacts "First Name" should be in "ascending" alphabetical order

@wip @10.3
Scenario: 10.3 Sort Contacts by Check-in
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   And the contacts "Last Name" should be in "ascending" alphabetical order
   When the user sorts the contacts by "Check-in" into "descending" alphabetical order
   Then the contacts "Check-in" should be in "descending" alphabetical order
   When the user sorts the contacts by "Check-in" into "ascending" alphabetical order
   Then the contacts "Check-in" should be in "ascending" alphabetical order


@wip@10.4
Scenario: 10.4 Sort Contacts by Company
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   And the contacts "Last Name" should be in "ascending" alphabetical order
   When the user sorts the contacts by "Company" into "descending" alphabetical order
   Then the contacts "Company" should be in "descending" alphabetical order
   When the user sorts the contacts by "Company" into "ascending" alphabetical order
   Then the contacts "Company" should be in "ascending" alphabetical order

@wip @10.5
Scenario: 10.5 Sort Contacts by Title
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   And the contacts "Last Name" should be in "ascending" alphabetical order
   When the user sorts the contacts by "Title" into "descending" alphabetical order
   Then the contacts "Title" should be in "descending" alphabetical order
   When the user sorts the contacts by "Title" into "ascending" alphabetical order
   Then the contacts "Title" should be in "ascending" alphabetical order

@wip @10.6
Scenario: 10.6 Sort Contacts by Lead Score
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   And the contacts "Last Name" should be in "ascending" alphabetical order
   When the user sorts the contacts by "Lead Score" into "descending" alphabetical order
   Then the contacts "Lead Score" should be in "descending" alphabetical order
   When the user sorts the contacts by "Lead Score" into "ascending" alphabetical order
   Then the contacts "Lead Score" should be in "ascending" alphabetical order
   	