#!/bin/bash

source "$HOME/.rvm/scripts/rvm"
[[ -s ".rvmrc" ]] && source .rvmrc
# Set "fail on error" in bash
set -e
#rvm list

rvm use 2.2.2

cd ios/cardscanner/
bundle install

case $TESTFEATURES in
	"")
		#bundle exec cucumber --format json --out cucumber.json features
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip features
		;;
	"Run Full Test Suite")
		#bundle exec cucumber --format json --out cucumber.json features
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip features
		;;
	"1. Request Trial")
		#bundle exec cucumber --format json --out cucumber.json features/01_cs_ios_request_trial.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @1
		;;
	"2. Login")
		#bundle exec cucumber --format json --out cucumber.json features/02_cs_ios_login.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @2
		;;
	"3. Landing Page")
		#bundle exec cucumber --format json --out cucumber.json features/03_cs_ios_landingpage.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @3
		;;
	"4. Contact Us")
		#bundle exec cucumber --format json --out cucumber.json features/04_cs_ios_contactus.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @4
		;;
	"5. About")
		#bundle exec cucumber --format json --out cucumber.json features/05_cs_ios_about.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @5
		;;
	"6. Report a Problem")
		#bundle exec cucumber --format json --out cucumber.json features/06_cs_ios_reportaproblem.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @6
		;;
	"7. Add Contact")
		#bundle exec cucumber --format json --out cucumber.json features/07_cs_ios_addcontact.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @7
		;;
	"8. Contacts")
		#bundle exec cucumber --format json --out cucumber.json features/08_cs_ios_contacts.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @8
		;;
	"9. Search Contacts")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @9
		;;
	"10. Sort Contacts")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @10
		;;
	"11. Filter Contacts")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @11
		;;
	"12. Deleting a Contact")
		#bundle exec cucumber --format json --out cucumber.json features/12_cs_ios_deleting_contacts.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @12
		;;
	"13. Contact Info Page")
		#bundle exec cucumber --format json --out cucumber.json features/13_cs_ios_contactinfo_page.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @13
		;;
	"14. Edit Contact Page")
		#bundle exec cucumber --format json --out cucumber.json features/14_cs_ios_editcontact_page.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @14
		;;
	"15. Topics")
		#bundle exec cucumber --format json --out cucumber.json features/15_cs_ios_topics.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @15
		;;
	"16. Follow-up Actions")
		#bundle exec cucumber --format json --out cucumber.json features/16_cs_ios_fua.feature
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @16
		;;
	"17. Contact Tabs")
		bundle exec cucumber --format json --out cucumber.json --tags ~@wip --tags @17
		;;
esac